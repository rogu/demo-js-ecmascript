/**
 * Example - function
 */

function add(a, b) {
    return a + b;
}

let params = [5, 4];

console.group('function');
console.log(add(...params));
console.groupEnd('function');

/**
 * Example - array
 */

const arr = [1, 2, 3];
const arrExtended = [...arr, 4, 5, 6];
console.group('array')
console.log(arrExtended);
console.groupEnd('array');

/**
 * Example - object
 */

const obj = { id: 1, name: 'joe', lastname: 'doe' };
const { name } = obj;
const { ...objCopy } = obj;
const { id, ...objCopyWithoutId } = obj;
// warunek
const valid = false;
const conditionParam = { ...obj, ...(!valid && { msg: 'any error' }) };

console.group('object');
console.log('oryginal', obj);
console.log('copy', objCopy, obj === objCopy);
console.log('copy without id', objCopyWithoutId);
console.log('name only', name);
console.log('condition', conditionParam);
console.groupEnd('object');
