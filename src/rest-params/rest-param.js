function format(str, ...args) {
    return str.replace(/{(\d+)}/g, (val, i) => args[i])
}

let msg = format(
    'The {0}st arg is a string, the {1} are {2}.',
    1,
    'rest',
    'unknown');

console.log(msg);
