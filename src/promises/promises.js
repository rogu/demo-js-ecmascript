function asyncFn() {
    return new Promise(function (response, reject) {
        setTimeout(() => {
            let rand = Math.round(Math.random());
            rand ? response(rand) : reject(rand);
        }, Math.random() * 5000);
    })
}

/**
 * Promise all
 */

function randomData() {
    return new Promise(function (response, reject) {
        setTimeout(() => {
            response(Date.now());
        }, Math.random() * 2000);
    })
}

async function severalPromise() {
    const [first, second] = await Promise.all([randomData(), randomData()]);
    return { first, second };
}

severalPromise().then(console.log);
