"use strict";

class Animal {
    constructor(firstName, age) {
        this.firstName = firstName;
        this.age = age;
    }

    get talk() {
        return "hau hau";
    }
}

let animal = new Animal("Szarik", 10);

/**
 * Extends
 */

class Man extends Animal {
    constructor(firstName, age, skinColor) {
        super(firstName, age);
        this.skinColor = skinColor;
    }

    get talk() {
        return "i'm a " + this.skinColor + ' ' + this.firstName + ", " + this.age + " " + this.constructor.name;
    }
}

let joe = new Man("Joe", 30, "white");

let creatures = [animal, joe];

creatures.forEach(function (item) {
    let creature = document.createElement('div');
    creature.innerHTML = `
        ${item.firstName} 
        (${item.constructor.name})
        say: <b>${item.talk}</b>
    `;
    creature.className = 'jumbotron';
    document.body.appendChild(creature);
});