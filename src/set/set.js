'use strict';

let set = new Set([1, 2, 3, 4, 4, 4, 5]);

set.add('six');
set.delete(2);

console.log('The set contains', set.size, 'elements.');
console.log('The set has 1:', set.has(1)); // true
console.log('The set has 8:', set.has(8)); // false

// values and keys are the same in a set
set.forEach((value, key) => console.log(value, key));

console.log(set.keys());
console.log(set.values());
console.log(set.entries());