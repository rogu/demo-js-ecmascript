let list = $('.list');

function logIn(user) {
    return new Promise(function (res, rej) {
        setTimeout(function () {
            if(user.login === 'admin' && user.password === 'admin') {
                res({access: true});
            } else {
                res({access: false});
            }
        }, 1000);
    });
}

function getItems() {
    return $.getJSON('https://api.emitter.pl/items');
}

function renderList(items) {
    list.text('');
    items.data.forEach(function (item) {
        let li = $("<li>").text(item.title);
        list.append(li);
    });
}

function* generator(userData) {
    let user = yield logIn(userData);
    if (!user.access) {
        throw new Error('you shall not pass');
    } else {
        let items = yield getItems();
        renderList(items);
    }
}

function run() {
    list.text('...waiting');
    async(generator({login: "admin", password: "admin"}));
}