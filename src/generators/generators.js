'use strict';

/**
 * Example 1
 */

function* greet() {
    const en = yield 'welcome';
    const de = yield 'gutten morgen';
    const pl = yield 'dzień dobry';
    yield [en, de, pl];
}

const greeter = greet(); // generator initialize
const one = greeter.next();
const two = greeter.next(one.value);
const three = greeter.next(two.value);
const greetings = greeter.next(three.value);
console.log(greetings.value);
const done = greeter.next();
console.log(done);


/**
 * Example 2
 * Iterator połączony z generatorem umożliwia sterowanie iteratorem
 */

function* range(start, end, step) {
    while (start < end) {
        yield start;
        start += step;
    }
}

for (let i of range(0, 10, 2)) {
    console.log(i);
}