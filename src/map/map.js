'use strict';

/**
 *  Example 1
 */

let map = new Map();
let today = new Date();

map.set('firstItem', 111);
// anything can be a key
map.set(today.toString(), 222);
map.set(today, 333);
map.delete(today.toString());

console.info('example 1 - create map');
console.log('The map contains', map.size, 'elements.');
console.log('The map has a today Date key:', map.has(today));
console.log('The map has a today string key:', map.has(today.toString()));
console.log('get today key:', map.get(today));


// values and keys
console.info('example 2 - iteration');
map.forEach((value, key) => console.log(value, key));


// iterable all (key, value)
console.info('all');
for (let value of map) {
    console.log(value);
}

// iterable entries (key, value)
console.info('entries');
for (let value of map.entries()) {
    console.log(value);
}

// iterable values
console.info('values');
for (let value of map.values()) {
    console.log(value);
}

// iterable keys
console.info('keys');
for (let value of map.keys()) {
    console.log(value);
}