/**
 * Object assign
 */

const o1 = { a: 1 };
const o2 = { b: 2 };
const o3 = { c: 3 };

const obj = Object.assign(o1, o2, o3);
console.log(obj);

// or with spread
const kris = { id: 1, job: 'dev', name: 'Kris' };
const mike = { ...kris, id: 2, name: 'Mike' };
console.warn(mike);


/**
 * Object enhancements
 */

const color = "red";
const speed = 10;
const drive = "move";

const car = {
    color,
    speed,
    ['can' + drive]: true,
    [drive]() {
        console.log("vroom");
    }
};

console.log(car.color);
console.log(car.speed);
console.log(car.canmove);
car.move();


/**
 * Object values
 */

let res = Object.values({ id: 1, name: 'joe' }).some((data) => /\d/.test(data));
console.log(res);

/**
 * Object entries
 */

const cars = {
    fiat: 100,
    tesla: 500,
    audi: 200
}

for (let [key, value] of Object.entries(cars)) {
    console.log(key, value);
}

const map = new Map(Object.entries(cars));
console.log(map.get('fiat'));
