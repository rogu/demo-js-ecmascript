'use strict';

/**
 * Example 1
 */

let square = x => x * x;
let add = (a, b) => a + b;
let pi = _ => 3.1415;

console.log(square(5));
console.log(add(3, 4));
console.log(pi());

/**
 * Example 2
 */

const deliveryBoy = {
    name: "John",
    handleMessage(message, handler) {
        handler(message);
    },
    recieve() {
        this.handleMessage('hello, ', msg => console.log(msg + this.name));
    }
};

deliveryBoy.recieve();

/**
 * example 3
 */

 const logger = x => y => z => x + y + z;
