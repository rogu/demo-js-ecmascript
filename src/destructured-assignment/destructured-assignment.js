'use strict';

/**
 * Example 1
 */

let [one, two] = [1, 2];
let {three, four} = {three: 3, four: 4};

console.info('example 1');
console.log('ex1:', one, two, three, four);

/**
 * Example 2
 */

let [first,,,last] = ['pierwszy', 'drugi', 'trzeci', 'czwarty'];
console.info('example 2');
console.log(first, last);

/**
 * Example 3
 */

let people = [
    {
        firstName: "John",
        email: "john@john.com"
    },
    {
        firstName: "Mike",
        email: "mike@mike.pl"
    },
    {
        firstName: "Elizabeth",
        email: "eliza@gmail.com"
    }
];

console.info('example 3');
people.forEach(({firstName, email}) => console.log('name:', firstName, 'email:', email));

/**
 * Example 4
 */

let [,mike] = people;

function logEmail({email}) {
    console.info('example 4');
    console.log('Mikes email:', email);
}

logEmail(mike);
