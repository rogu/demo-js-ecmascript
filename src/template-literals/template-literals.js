'use strict';

/**
 * Example 1
 */

let person = {name: 'John Smith'};
let tpl = `My name is ${person.name}.`;

console.log(tpl);

/**
 * Example 2
 */

const x = 10,
    y = 20,
    equation = `${ x } + ${ y } = ${ x + y }`;

console.log(equation);

/**
 * Example 3
 */

function tag(strings, ...values) {
    console.log('s', strings);
    console.log('v', values);
    if (values[0] > 20) {
        values[1] = "tired";
    }

    return `${strings[0]}${values[0]}${strings[1]}${values[1]}`
}

const message = tag`It's ${new Date().getHours()} I'm ${'ready'}`;

console.log(message);
