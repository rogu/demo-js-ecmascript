'use strict';

/**
 * Example 1
 */

let funcs = [];
// let tworzy nową instancją w każdej iteracji
for (let j of [4, 5, 6]) {
    funcs.push(function () {
        return j;
    });
}

// zmienna "j" nie istnije tutaj
try {
    console.log(j);
} catch (err) {
    //console.error('err', err);
}

for (const func of funcs) {
    console.log('--', func()); // 4, 5, 6
}

/**
 * Example 2
 */

let msg = 'hi';

{
    let msg = 'bye'; // blok tworzy zakres dla zmiennej let
}

console.log('msg is', msg); // hi


/**
 * Example 3
 */

const contacts = [
    {name: "Tomasz", mobile: 1111},
    {name: "Michał", mobile: 222222},
    {name: "Kamil", mobile: 33333333}
];

const display = document.querySelector('.display');
const group = document.querySelector('.group1');

for (let i = 0; i < contacts.length; i++) {
    let person = document.createElement("li");
    person.innerHTML = `${contacts[i].name} <button>show phone</button>`;
    person.className = 'list-group-item list-group-item-action';
    person.addEventListener('click', function () {
        display.innerHTML = contacts[i].mobile;
    });
    group.appendChild(person);
}