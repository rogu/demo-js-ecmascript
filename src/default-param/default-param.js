'use strict';

/**
 * Example 1
 */

function sayMsg(msg = 'This is a default message.') {
    console.log(msg);
}

sayMsg();
sayMsg('This is a different message!');
sayMsg();

/**
 * Example 2
 */

const showError = (msg, cb = (param) => console.log(param)) => cb(msg);
showError('error1');
showError('error2', alert);
showError('error3', console.warn);
