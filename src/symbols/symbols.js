"use strict";

/**
 * Example 1
 */

const firstName = Symbol('foo foo');
const student = {
    [firstName]: "bar bar"
};

console.log(student[firstName]);

// ---------

Object.defineProperty(student, firstName, {
    writable: false
});

try {
    student[firstName] = '111111111';
} catch (err) {
    console.error(err);
}


/**
 * Example 2
 */

const nameSymbol = Symbol();

class Animal {
    constructor(kind, age) {
        this.kind = kind;
        this.age = age;
    }

    set name(name) {
        this[nameSymbol] = name;
    }

    get name() {
        return this[nameSymbol];
    }
}

let animal = new Animal("pies,", 10);
console.log(animal.kind, animal.age);
animal.name = 'szarik';
console.log(animal); // Animal {kind: "pies,", age: 10, Symbol(): "szarik"}
